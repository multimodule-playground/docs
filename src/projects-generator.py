import argparse
import json
import os
import logging

from jinja2 import Environment, FileSystemLoader, select_autoescape

logger = logging.getLogger(__name__)
logging.basicConfig(encoding='utf-8', level=logging.DEBUG)

env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(),
    trim_blocks=True,
    lstrip_blocks=True
)

parser = argparse.ArgumentParser(
    prog=''
)
parser.add_argument('--changes_file_name', type=str)
args = parser.parse_args()

changes_file_name_suffix = args.changes_file_name


def generate_changes(project_id, project_changes_path):
    with open(project_changes_path) as project_changes_file:
        project_changes = json.load(project_changes_file)
    generated_changes = env.get_template('changes.j2').render(changes=project_changes)
    with open('doc-project/docs/projects/' + str(project_id) + '/' + 'changes.md', 'w') as generated_changes_file:
        generated_changes_file.write(generated_changes)


def process_submodules_recursive(project_id, parent_name, submodules_tree, relative_path, submodules_path):
    submodules = []
    submodules_with_children = []
    submodules_without_children = []
    for key, value in submodules_tree.items():
        submodules.append(key)
        if len(value) != 0:
            submodules_with_children.append(key)
        else:
            submodules_without_children.append(key)
    submodules.sort()
    submodule_path = '/'.join(relative_path)
    logger.info('Processing submodules in: %s', submodule_path)

    if not os.path.exists(submodule_path):
        os.mkdir(submodule_path)

    logger.debug('Found #%d leaf modules in submodule.', len(submodules_with_children))
    for submodule_leaf in submodules_without_children:
        logger.info('Processing leaf module: %s', submodule_leaf)

        submodule_leaf_path = os.path.join(submodule_path, submodule_leaf)
        if not os.path.exists(submodule_leaf_path):
            os.mkdir(submodule_leaf_path)

        submodule_info_leaf_path = '/'.join(
            ['projects', project_id]
            + submodules_path
            + [submodule_leaf, 'module-info.json']
        )
        submodule_info_exists = False
        if os.path.exists(submodule_info_leaf_path):
            logger.info('Module contains module-info.json.')
            submodule_info_exists = True
            with open(submodule_info_leaf_path) as submodule_info_file:
                submodule_info = json.load(submodule_info_file)

            generated_module_info_path = os.path.join(submodule_leaf_path, 'module-info.md')
            generated_module_info = env.get_template('module-info.j2').render(module_info=submodule_info)
            with open(generated_module_info_path, 'w') as generated_module_info_file:
                generated_module_info_file.write(generated_module_info)
        else:
            logger.debug('module-info.json was not found on path: %s', submodule_info_leaf_path)

        generated_submodule_index_path = os.path.join(submodule_leaf_path, 'index.md')
        if not os.path.exists(generated_submodule_index_path):
            logger.info('Creating index.md...')
            generated_submodule_index = (env.get_template('submodule-leaf-index.j2')
                                         .render(submodule_info_exists=submodule_info_exists))
            with open(generated_submodule_index_path, 'w') as generated_submodule_index_file:
                generated_submodule_index_file.write(generated_submodule_index)
        else:
            logger.debug('index.md already exists on path: %s', generated_submodule_index_path)

        generated_module_description_path = os.path.join(submodule_leaf_path, 'module_description.md')
        if not os.path.exists(generated_module_description_path):
            logger.info('Creating module_description.md...')
            generated_module_description = env.get_template('module-description.j2').render(header=submodule_leaf)
            with open(generated_module_description_path, 'w') as generated_module_description_file:
                generated_module_description_file.write(generated_module_description)
        else:
            logger.debug('module_description.md already exists on path: %s', generated_module_description_path)

    generated_submodule_index_path = '/'.join(relative_path + ['index.md'])
    if not os.path.exists(generated_submodule_index_path):
        logger.info('Creating index.md...')
        generated_submodule_index = (env.get_template('submodule-index.j2')
                                     .render(submodule_info_exists=False))
        with open(generated_submodule_index_path, 'w') as generated_submodule_index_file:
            generated_submodule_index_file.write(generated_submodule_index)
    else:
        logger.debug('index.md already exists on path: %s', generated_submodule_index_path)

    generated_module_description_path = '/'.join(relative_path + ['module_description.md'])
    if not os.path.exists(generated_module_description_path):
        logger.info('Creating module_description.md...')
        generated_module_description = env.get_template('module-description.j2').render(header=parent_name)
        with open(generated_module_description_path, 'w') as generated_module_description_file:
            generated_module_description_file.write(generated_module_description)
    else:
        logger.debug('module_description.md already exists on path: %s', generated_module_description_path)

    generated_submodules_path = '/'.join(relative_path + ['submodules.md'])
    generated_submodules = env.get_template('submodules.j2').render(submodules=submodules)
    logger.info('Creating submodules.md')
    with open(generated_submodules_path, 'w') as generated_submodules_file:
        generated_submodules_file.write(generated_submodules)

    logger.debug('Found #%d submodules with children.', len(submodules_with_children))
    if len(submodules_with_children) != 0:
        logger.info('Processing child submodule...')
        for submodule in submodules_with_children:
            process_submodules_recursive(project_id,
                                         submodule,
                                         submodules_tree[submodule],
                                         relative_path + [submodule],
                                         submodules_path + [submodule])


def process_submodules(project, project_dir_path):
    project_id = str(project['id'])
    submodules_tree_path = os.path.join('projects', project_id + '_tree.json')
    if not os.path.exists(submodules_tree_path):
        logger.info('Project does not have any submodules.')

        if not os.path.exists(project_dir_path):
            logger.debug('Project dir does not exists, creating a new directory...')
            os.mkdir(project_dir_path)

        generated_index_path = os.path.join(project_dir_path, 'index.md')
        if not os.path.exists(generated_index_path):
            logger.info('Generating project index.md file.')
            generated_index = env.get_template('project-index.j2').render()
            with open(generated_index_path, 'w') as generated_index_file:
                generated_index_file.write(generated_index)
        else:
            logger.debug('index.md already exists on path: %s', generated_index_path)

        generated_module_description_path = os.path.join(project_dir_path, 'module_description.md')
        if not os.path.exists(generated_module_description_path):
            generated_module_description = env.get_template('module-description.j2').render(header=project['name'])
            with open(generated_module_description_path, 'w') as generated_module_description_file:
                generated_module_description_file.write(generated_module_description)
        else:
            logger.debug('module_description.md already exists on path: %s', generated_module_description_path)

        return

    if not os.path.exists(project_dir_path):
        logger.debug('Project dir does not exists, creating a new directory...')
        os.mkdir(project_dir_path)

        generated_index_path = os.path.join(project_dir_path, 'index.md')
        if not os.path.exists(generated_index_path):
            generated_index = env.get_template('module-index.j2').render()
            logger.info('Generating project index.md file.')
            with open(generated_index_path, 'w') as generated_index_file:
                generated_index_file.write(generated_index)
        else:
            logger.debug('index.md already exists on path: %s', generated_index_path)

    with open(submodules_tree_path) as submodules_tree_file:
        submodules_tree = json.load(submodules_tree_file)
    logger.debug('Found #%d submodules', len(submodules_tree))
    process_submodules_recursive(project_id, project['name'], submodules_tree, [project_dir_path], [])


def process_project(project):
    project_dir_path = 'doc-project/docs/projects/' + str(project['id'])
    logger.debug('Processing submodules of a project.')
    process_submodules(project, project_dir_path)


def expand_project_tree(project_tree):
    output_tree = {}
    for key in project_tree:
        output_tree[key] = {}
        value = project_tree[key]
        if isinstance(value, int):
            module_tree_path = 'projects/' + str(value) + '_tree.json'
            if os.path.exists(module_tree_path):
                with open(module_tree_path) as module_tree_file:
                    module_tree = json.load(module_tree_file)
                    project_tree[key] = {'module_tree': module_tree, 'project_id': value}
        else:
            expand_project_tree(value)


def main():
    logger.info('Loading projects.json')
    with open('projects/projects.json') as projects_file:
        projects = json.load(projects_file)

    projects_dir_path = 'doc-project/docs/projects/'
    if not os.path.exists(projects_dir_path):
        os.mkdir(projects_dir_path)

    logger.debug('Found #%d projects...', len(projects))
    for project in projects:
        logger.info('Processing project #%d (%s)', project['id'], project['name'])
        process_project(project)

    logger.info('Loading project-tree.json')
    with open('projects/project-tree.json') as project_tree_file:
        project_tree = json.load(project_tree_file)

    logger.info('Expanding project-tree...')
    # expand_project_tree(project_tree)
    logger.info('Created projects side navigation file.')
    generated_projects_side_nav = env.get_template('projects.j2').render(
        project_tree=project_tree['multimodule-playground'],
        default_indent=1
    )
    with open('doc-project/docs/nav.md', 'w') as generated_projects_side_nav_file:
        generated_projects_side_nav_file.write(generated_projects_side_nav)


main()
